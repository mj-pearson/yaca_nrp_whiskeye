# YACA_nrp_whiskeye

The cognitive control architecture for the WhiskEye robot model on the NRP

### Make sure to add /lib and /model to PYTHONPATH

With an instance of the WhiskEye running on the NRP (including a fully functional plugin) simply cd into /models and:
> ./run_sim.sh

We log a bag file of the most important topics (as listed in log_topic_list_sim) by added the argument log:
> ./run_sim.sh log

At the end of a run the bag file is time stamped and saved in the home directory

The main python script is model_main.py which sets up the network of other modules and initiates the ros communication.

I have included the whiskeye_msgs ROS package into the /lib folder
