
import numpy as np
import time
import copy

from yaca_support import *
from yaca_reaff_latest import *

class ReaffFilter:

	def __init__(self, pars):

		self.pars = pars

		# get filter coeffs
		reaff_ = get_reaff()
		self.reaff = create_empty_array_of_shape(pars.shape_lo_2)
		i = 0
		for row in self.pars.rows:
			for col in self.pars.cols:
				for dim in self.pars.dims:
					b = reaff_[i]
					i += 1
					# b is the filter to be applied to the dtheta signal, but
					# we'll be processing theta so let's add a derivative term
					b1 = np.concatenate((b, [0.0]))
					b2 = np.concatenate(([0.0], b))
					b = b1 - b2
					f = ArrayFilter([1], b, [1])
					self.reaff[row][col][dim] = f

	def tick(self, theta_f, xy):

		xy_out = copy.copy(xy)

		for row in self.pars.rows:
			for col in self.pars.cols:
				f = self.reaff[row][col]
				for i in range(0, self.pars.fS_scale):
					theta = theta_f[i, row, col]
					x = xy[i, row, col, 0]
					y = xy[i, row, col, 1]
					xy_out[i, row, col, 0] = x - f[0].run_simple(theta)
					xy_out[i, row, col, 1] = y - f[1].run_simple(theta)

		return xy_out
