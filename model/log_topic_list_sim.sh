#!/bin/bash

# explicit list
echo -e "/model/cam0/compressed"
echo -e "/model/cam1/compressed"
echo -e "/whiskeye/head/bridge_u"
echo -e "/whiskeye/head/theta_cmd"
echo -e "/model/xy_q"
echo -e "/model/protracting"
echo -e "whiskeye/body/pose"
echo -e "whiskeye/head/contact_head"
echo -e "whiskeye/head/contact_world"
#echo -e "/whiskeye/body/odom"
exit 0

# initial list
#cp /tmp/topics_all /tmp/topics
rostopic list > /tmp/topics

# pare down
cat /tmp/topics \
	| grep -v "model/contacted" \
	| grep -v "model/fin" \
	| grep -v "model/head_center_WORLD" \
	| grep -v "model/ior" \
	| grep -v "model/mapval" \
	| grep -v "model/max_macon" \
	| grep -v "model/sausage" \
	| grep -v "model/theta" \
	| grep -v "model/tick" \
	| grep -v "model/visited" \
	| grep -v "model/xy$" \
	| grep -v "/rosout" \
	| grep -v "whiskeye/body/[^o][^d]" \
	| grep -v "whiskeye/gui/cmd_vel" \
	| grep -v "whiskeye/head/cam" \
	| grep -v "whiskeye/head/neck_cmd" \
	| grep -v "whiskeye/head/neck_cmd" \
	| grep -v "whiskeye/head/xy" \
	| grep -v "/tf" \
	| grep -v "/rqt" \
	> /tmp/topics_log


# add some back
echo -e "/whiskeye/body/cmd_vel" >> /tmp/topics_log

# report
echo "\n----------------------------------------"
cat /tmp/topics_log
