#!/usr/bin/python

# import
import numpy as np
from kc_interf import *
from model_pars import *

# instantiate model pars
pars = SystemPars()

# instantiate whiskers kc (in HEAD FOR)
kc_w = kc_whiskers(pars)



#######################################################

# AT EACH SAMPLE TIME - load all your data and cycle
# through the remainder of this file with it at each
# sample

#######################################################

######################
# LOAD ONE SAMPLE
######################

# NB: the suffix "lo" refers to the sample rate, down
# from 500Hz to 50Hz IIRC. for the sake of this code,
# the sample rate is immaterial, so here it's just for
# naming consistency with code from the yaca codebase.

# load your config data
# (create dummy data)
theta_lo = np.ones(pars.shape_lo) * np.pi * 0.25

# load your xy data
# (create dummy data)
xy_lo = np.ones(pars.shape_lo_2)

#######################################################

######################
# PROCESS FOR TIP & NORM IN HEAD
######################

# configure whiskers as specified by theta
# see ModuleSensory.py line 343+

# update kc_w
for row in pars.rows:
	for col in pars.cols:

		# lay theta (whisker angle) in to kc_w
		theta = theta_lo[row][col]
		kc_w[row][col].link[5].angle = theta

		# compute whisker tip location in HEAD
		# NB: at time of writing, WHISKER has zero rotation wrt ROW if
		# theta is zero (i.e. the whisker is straight out), which means
		# the whisker tip is at x=0, y=0, z=l. we could easily add 90
		# degrees to theta, and place the tip at x=l, y=0, z=0. it may
		# or may not be more consistent (and thus helpful) to do this.
		xyz_tip_W = np.array([0.0, 0.0, pars.whisker_length[col]]) # whisker tip in WHISKER
		xyz_tip_H = kc_w[row][col].changeFrameAbs(KC_FRAME_WHISKER, KC_FRAME_PARENT, xyz_tip_W) # whisker tip in HEAD
		
		# now, to get the normal, you also need to take another point,
		# somewhere offset from tip in the direction (x, y). that's
		# straightforward...
		xy_lo_rc = xy_lo[row][col]
		dxyz = np.array([xy_lo_rc[0], xy_lo_rc[1], 0.0])
		xyz_off_W = xyz_tip_W + dxyz # whisker offset point in WHISKER
		xyz_off_H = kc_w[row][col].changeFrameAbs(KC_FRAME_WHISKER, KC_FRAME_PARENT, xyz_off_W) # whisker offset point in HEAD
		
		# finally, the normal is the difference
		normvec_H = xyz_off_H - xyz_tip_H
		
		# display for fun
		print(normvec_H)
		
		# NB: the magnitude of normvec_H encodes the magnitude of the
		# sensed deflection.
		#
		# NB2: I've no idea if the sense is correct; if the normals
		# appear to be in the wrong direction (mostly, inwards towards
		# the snout centre) just reverse them all. It's also possible
		# they are rotated, but less likely. See what you get.

#######################################################




