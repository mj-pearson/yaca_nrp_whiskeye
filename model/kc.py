
import numpy as np
import math
import copy

# internal value used by the kc
KC_ANGLE_UNCONSTRAINED = 1000.0 * np.pi

# identifier of the frame that is the parent to
# the zeroth frame in the kc stack.
KC_FRAME_PARENT = -1

# identifier of zeroth frame, which is the one
# that is assumed to be mobile in PARENT
KC_FRAME_ZERO = 0

# fixed values
KC_PUSH_FLAG_IMPULSE = 1
KC_PUSH_FLAG_VELOCITY = 2
KC_PUSH_FLAG_NO_PARENT_TRANSLATION = 4
KC_PUSH_FLAG_NO_PARENT_ROTATION = 8
KC_PUSH_FLAG_NO_INTERNAL_MOVEMENT = 16

# default values
KC_TICK_SEC=0.02
KC_PUSH_FLAGS_DEFAULT=KC_PUSH_FLAG_VELOCITY
KC_PUSH_LINK_DEFAULT=0
KC_SHOW_CONFIG_IF_ACTIVE=False
KC_ACTIVE_TIMEOUT=3

# function to initialise default values
def kc_init(tick, flags, link):
	global KC_TICK_SEC, KC_PUSH_FLAGS_DEFAULT, KC_PUSH_LINK_DEFAULT
	KC_TICK_SEC=tick
	KC_PUSH_FLAGS_DEFAULT=flags
	KC_PUSH_LINK_DEFAULT=link

# debug state
KC_DEBUG=False



###################################### HELPER FCNS #######################################

def kc_rotate( point_in, axis, angle ):

	point_out = np.array([0.0, 0.0, 0.0])

	c = np.cos( angle )
	s = np.sin( angle )

	if 'x' in axis:
		point_out[1] = c*point_in[1] - s*point_in[2]
		point_out[2] = s*point_in[1] + c*point_in[2]
		point_out[0] = point_in[0]

	elif 'y' in axis:
		point_out[2] = c*point_in[2] - s*point_in[0]
		point_out[0] = s*point_in[2] + c*point_in[0]
		point_out[1] = point_in[1]

	elif 'z' in axis:
		point_out[0] = c*point_in[0] - s*point_in[1]
		point_out[1] = s*point_in[0] + c*point_in[1]
		point_out[2] = point_in[2]

	else:
		raise ValueError( 'invalid axis' )

	return point_out

def kc_rotate_fwd( lhs, link ):
	return kc_rotate( lhs, link.axis, link.angle )

def kc_rotate_rev( lhs, link ):
	return kc_rotate( lhs, link.axis, -link.angle )

###################################### OBJECT #######################################

class KinematicPush:

	def __init__(self, name=None):

		if name is None:
			name = 'unnamed'

		self.name = name
		self.flags = KC_PUSH_FLAGS_DEFAULT
		self.link = KC_PUSH_LINK_DEFAULT
		self.pos = np.array([0.0, 0.0, 0.0])
		self.vec = np.array([0.0, 0.0, 0.0])

	def resolve(self):

		# if is_velocity, convert to position
		if self.flags & KC_PUSH_FLAG_VELOCITY:

			push = KinematicPush(self.name)
			push.flags = self.flags & ~(KC_PUSH_FLAG_VELOCITY) | KC_PUSH_FLAG_IMPULSE
			push.link = self.link
			push.pos = copy.copy(self.pos)
			push.vec = self.vec * KC_TICK_SEC
			return push

		else:

			# otherwise, make a copy and return that, so
			# we don't change our input
			return copy.copy(self)

###################################### OBJECT #######################################

class KinematicLink:

	def __init__( self, name, trans, axis, angle_ini, pars ):

		self.name = name
		self.translation = trans
		self.axis = axis
		self.angle = angle_ini

		if pars is None:
			self.angle_min = angle_ini
			self.angle_max = angle_ini
			self.angle_fix = angle_ini
		else:
			self.angle_min = pars[0]
			self.angle_max = pars[1]
			self.angle_fix = pars[2]

	def toParentAbs(self, pos):
		pos = kc_rotate_fwd(pos, self)
		return pos + self.translation

	def toParentRel(self, direction):
		return kc_rotate_fwd(direction, self)

	def fromParentAbs(self, pos):
		pos = pos - self.translation
		return kc_rotate_rev(pos, self)

	def fromParentRel(self, direction):
		return kc_rotate_rev(direction, self)

	def push(self, pushpos_i, pushvec):

		# pushpos: the "push point", the point we want to move
		# pushvec: the "push vector", the amount we want it to move by
		#
		# "i": initial (before the push is applied)
		# "f": final (after the push is applied)
		#
		# the implementation aims to move the push point onto the
		# "pushed point", defined as:
		#
		# pushpos_f = pushpos_i + pushvec
		#
		# in the FOR defined by this link.
		#
		# owing to constraints (joint axis, angle limits) we can't generally
		# find a link movement that achieves this. instead, we do the best we
		# can, and can define the "actual pushed point" as:
		#
		#	pushpos_f_act = pushpos_i + pushvec_act
		#
		# having done this, we can pass up the "remaining push vector", defined
		# as:
		#
		#	pushvec_rem = pushvec - pushvec_act
		#
		# into the parent link for further processing.

		# debug state
		global KC_DEBUG

		#	compute proposed pushed point
		pushpos_f = pushpos_i + pushvec

		#	amount we will change joint angle by
		angle_delta = 0.0

		# if not fixed
		if self.angle_fix == KC_ANGLE_UNCONSTRAINED:

			# calculate movement of link's joint required to bring
			# pushpos_i as close to pushpos_f as possible, given
			# only this joint's axis of rotation
			if self.axis == 'x':
				angle_delta = math.atan2(pushpos_f[2], pushpos_f[1]) - math.atan2(pushpos_i[2], pushpos_i[1])
			if self.axis == 'y':
				angle_delta = math.atan2(pushpos_f[0], pushpos_f[2]) - math.atan2(pushpos_i[0], pushpos_i[2])
			if self.axis == 'z':
				angle_delta = math.atan2(pushpos_f[1], pushpos_f[0]) - math.atan2(pushpos_i[1], pushpos_i[0])

		# if fixed
		else:

			# approach angle_fix
			angle_delta = (self.angle_fix - self.angle) * 0.1

		# minimise absolute value, solely for computational stability
		if angle_delta > math.pi:
			angle_delta -= 2 * math.pi
		if angle_delta < -math.pi:
			angle_delta += 2 * math.pi

		# store old joint angle
		angle_i = self.angle

		# propose new joint angle based on this result
		angle_f = self.angle + angle_delta

		# debug
		if KC_DEBUG:
			print "angle_i", angle_i
			print "angle_f", angle_f
			print "angle_delta", angle_delta

		# if not fixed
		if self.angle_fix == KC_ANGLE_UNCONSTRAINED:

			# constrain that proposed joint angle by joint limits
			if not self.angle_min == KC_ANGLE_UNCONSTRAINED:
				angle_f = max(angle_f, self.angle_min)
			if not self.angle_max == KC_ANGLE_UNCONSTRAINED:
				angle_f = min(angle_f, self.angle_max)

			# recalculate angle_delta after these constraints have been applied
			angle_delta = angle_f - angle_i

		# debug
		if KC_DEBUG:
			print "angle_f (post)", angle_f
			print "angle_delta (post)", angle_delta

		# perform the rotation to see how much pushpos has moved
		pushpos_f_act = kc_rotate(pushpos_i, self.axis, angle_delta)

		# and compute actual pushvec
		pushvec_act = pushpos_f_act - pushpos_i

		# and remaining pushvec
		#
		# NB: this is _still_ in the old FOR, before the link
		# joint is adjusted (i.e. with "angle_i" in the joint).
		pushvec_rem = pushvec - pushvec_act

		# debug
		if KC_DEBUG:
			print "pushpos_f_act", pushpos_f_act
			print "pushvec_rem", pushvec_rem

		# transform remaining pushvec into parent frame, for chaining,
		# through the old state of the link
		self.angle = angle_i
		pushvec_rem_parent = self.toParentRel(pushvec_rem)

		# lay in actual new angle to implement the push operation
		self.angle = angle_f

		# transform push point into parent frame too. note that
		# it's in the same place as it was - the link has moved,
		# but the push point (e.g. fovea) is still at the same
		# location within it.
		pushpos_i_parent = self.toParentAbs(pushpos_i)

		# return these for propagation
		return (pushpos_i_parent, pushvec_rem_parent)

###################################### OBJECT #######################################

class KinematicChain:

	def __init__( self, link_desc_array, holonomic=None ):

		# data
		self.debug = False
		self.active = 0
		self.link = []
		self.linkNames = []
		self.push_i = 0 # used for debugging

		# set holonomic
		self.holonomic = False
		if not holonomic is None:
			self.holonomic = holonomic

		# zero pose change
		if self.holonomic:
			self.poseChange = np.array([0.0, 0.0, 0.0])
		else:
			self.poseChange = np.array([0.0, 0.0])

		# create links
		for link_desc in link_desc_array:

			# extract init data
			name = link_desc[0]
			trans = link_desc[1]
			axis = link_desc[2]
			angle_ini = link_desc[3]
			pars = link_desc[4]

			# add link itself
			self.link.append(KinematicLink(name, trans, axis, angle_ini, pars))
			self.linkNames.append(name)

		# data
		self.linkCount = len(self.link)

		# identifier of "tip" link
		self.linkTip = self.linkCount - 1

	def zeroPose(self):

		# zero pose
		pose = self.getPose()
		if np.linalg.norm(self.link[KC_FRAME_ZERO].translation) != 0.0 or self.link[KC_FRAME_ZERO].angle != 0.0:
			#print "zeroPose()"
			self.link[KC_FRAME_ZERO].translation = np.array([0.0, 0.0, 0.0])
			self.link[KC_FRAME_ZERO].angle = 0.0

	def getPose(self):

		# get pose
		return (self.link[KC_FRAME_ZERO].translation[0:2], self.link[KC_FRAME_ZERO].angle)

	def accumPoseChange(self, dpose):

		# just add it up
		self.poseChange += dpose

	def getPoseChange(self):

		# get accumulated pose change
		poseChange = copy.copy(self.poseChange)

		# zero pose change
		if self.holonomic:
			self.poseChange = np.array([0.0, 0.0, 0.0])
		else:
			self.poseChange = np.array([0.0, 0.0])

		# ok
		return poseChange

	def setPose(self, pose):

		# set pose
		self.link[KC_FRAME_ZERO].translation = [pose[0], pose[1], 0.0]
		self.link[KC_FRAME_ZERO].angle = pose[2]

	def addPoseChange(self, poseChange):

		# extract
		dx = poseChange[0]
		dy = poseChange[1]
		dtheta = poseChange[2]

		# do rotation
		self.link[KC_FRAME_ZERO].angle += dtheta

		# rotate translation by resulting pose angle
		theta = self.link[KC_FRAME_ZERO].angle
		c = np.cos(theta)
		s = np.sin(theta)
		dx_ = c * dx - s * dy
		dy_ = s * dx + c * dy

		# do translation
		self.link[KC_FRAME_ZERO].translation += np.array([dx_, dy_, 0.0])

	def setConfig( self, config ):

		# set config
		for i in range(1, len(self.link)):
			self.link[i].angle = config[i-1]

	def setConfigIfInactive(self, config):

		# if we are being pushed, ignore the updated config for now, because
		# it might destabilise closed-loop control. once we're stationary, we
		# can update the config whenever it reaches us
		if self.active > 0:
			self.active -= 1
			return True
		else:
			self.setConfig(config)
			if KC_SHOW_CONFIG_IF_ACTIVE:
				mismatch = False
				for i in range(1, len(self.link)):
					mismatch |= self.link[i].angle != config[i-1]
				if mismatch:
					print "setConfig(", config[0], config[1], config[2], config[3], ")"
			return False

	def getConfig(self):
		config = []
		for i in range(1, len(self.link)):
			config.append(self.link[i].angle)
		return config

	def getState( self ):
		return [self.getPose(), self.getConfig()]

	def changeFrameAbs( self, inFrame, outFrame, pos ):

		# ascend chain
		if outFrame > inFrame:
			for i in range(inFrame, outFrame):
				link = self.link[i+1]
				pos = link.fromParentAbs(pos)

		# descend chain
		elif inFrame > outFrame:
			for i in range(inFrame, outFrame, -1):
				link = self.link[i]
				pos = link.toParentAbs(pos)

		return pos

	def changeFrameRel( self, inFrame, outFrame, pos ):

		# ascend chain
		if outFrame > inFrame:
			for i in range(inFrame, outFrame):
				link = self.link[i+1]
				pos = link.fromParentRel(pos)

		# descend chain
		elif inFrame > outFrame:
			for i in range(inFrame, outFrame,-1):
				link = self.link[i]
				pos = link.toParentRel(pos)

		return pos

	def push(self, push):

		# debug
		self.push_i += 1

		# pushes in link KC_FRAME_PARENT are null
		if push.link == KC_FRAME_PARENT:
			return

		# if push is a velocity, reduce it to a position change
		push = push.resolve()

		# if push is empty, no effect
		if np.linalg.norm(push.vec) == 0.0:
			return

		# report
		#if np.linalg.norm(push.vec):
		#	print push.name, push.vec

		# mark active
		self.active = KC_ACTIVE_TIMEOUT

		#	This function applies the push vector "pushvec" to the pushpoint
		#	"pushpos" in the link frame "pushlink". This results in a change
		#	in the state of the KC which will include changes
		#	to the rotation angle of every link in the chain as well as a
		#	change to the translation vector of the zeroth link, which is
		#	assumed to be the unconstrained link (usually, ZERO is mobile
		#	in the x/y plane of its parent, PARENT).
		#
		#	The changes to the constrained links are expected to be
		#	implemented using position servos, so the values for their
		#	configuration stored in the chain are sufficient for the caller
		#	to implement the changes to the model in hardware. For the
		#	unconstrained link, however, the hardware uses velocity servos
		#	(i.e. the wheel speed controllers). The caller must observe the
		#	changes to kc across any pushes that are applied to recover these.
		#
		#	Thus, use this function as follows:
		#
		#	kc.zeroPose()
		#	...apply one or more pushes...
		#	state = kc.getState()
		#
		#	The result in "state" includes the change to ZERO frame across
		#	the pushes and the new configuration of the remaining joints.
		#
		#	NB: the terms "frame" and "link" are used pretty much
		#	interchangeably throughout this document. The "frame" is the
		#	frame-of-reference (FOR), which is associated with a physical
		#	link (an actual rigid member, jointed to others such).

		# if NO_INTERNAL_MOVEMENT, there's nothing to do here
		if not (push.flags & KC_PUSH_FLAG_NO_INTERNAL_MOVEMENT):

			# walk the push down the chain to the zeroth link
			for i in range(push.link, 0, -1):
				(push.pos, push.vec) = self.link[i].push(push.pos, push.vec)

		#	After walking the servo joints of the KC, we will - in general
		#	- be left with a non-zero push-vector in ZERO. Servicing this
		#	requires a movement of ZERO in PARENT (provided ZERO is mobile).
		#
		#	The treatment is different for platforms with and without
		#	non-holonomic constraints. Holonomic (omnidirectional)
		#	platforms can minimise their total movement by treating
		#	the final left-over vector as:
		#
		#		a) rotate until pushvec points away from or towards
		#			platform centre point.
		#		b) move platform centre point to zero pushvec.
		#
		#	Noting that (a) is the same operation as is used in pushing
		#	the servo joints, we can service it with the same code.
		#
		#	Non-holonomic platforms cannot do this, because (b) may
		#	not be in a direction they can move. Instead, they do:
		#
		#		a) rotate until pushvec is aligned with drive direction.
		#		b) move platform centre point to zero pushvec.
		#
		#	Finally, the above treatment for non-hol platforms is not stable
		#	if the dr component is not respected. So, for gaze changes only
		#	(dr constrained to be zero), we use a different approach.

		# if NO_PARENT_ROTATION, there's nothing to do here
		if push.flags & KC_PUSH_FLAG_NO_PARENT_ROTATION:
			return

		# debug state
		global KC_DEBUG
		#KC_DEBUG = self.push_i == 1096

		# debug
		if KC_DEBUG:
			print "----\n"

		# if holonomic
		if self.holonomic:

			# store initial orientation of mobile frame
			zero_ori_i = self.link[KC_FRAME_ZERO].angle

			# push ZERO in PARENT
			if KC_DEBUG:
				print "(in ZERO)"
				print "push.pos", push.pos
				print "push.vec", push.vec
			(push.pos, push.vec) = self.link[KC_FRAME_ZERO].push(push.pos, push.vec)
			if KC_DEBUG:
				print "(in PARENT)"
				print "push.pos", push.pos
				print "push.vec", push.vec

			# if NO_PARENT_TRANSLATION, we're done
			if push.flags & KC_PUSH_FLAG_NO_PARENT_TRANSLATION:
				return

			# store final orientation of mobile frame
			zero_ori_f = self.link[KC_FRAME_ZERO].angle

			# get the pose change in parent
			poseChange_PARENT = push.vec

			# finally, the remaining translation is now in PARENT, after the
			# above call to push(). we just need to pop it back up to ZERO in
			# case the caller wants to use it as a command for the robot.
			self.link[KC_FRAME_ZERO].angle = zero_ori_i
			poseChange_ZERO = self.link[KC_FRAME_ZERO].fromParentRel(poseChange_PARENT)
			self.link[KC_FRAME_ZERO].angle = zero_ori_f

			# eliminate any z movement because it's not physically doable
			poseChange_PARENT[2] = 0.0
			poseChange_ZERO[2] = 0.0

			# measure how much ZERO was rotated by the push
			# element 2 of poseChange is theta (x, y, theta)
			dtheta = zero_ori_f - zero_ori_i

			# this gives us the pose change of ZERO in PARENT as:
			# (dx=poseChange[0], dy=poseChange[1], dtheta)
			#
			# dtheta is already implemented (we measured it, above...)
			# dx, dy we implement by moving ZERO in PARENT; thus, to
			# implement this physically, the caller should use the
			# recipe described in the comments further up to look at
			# how ZERO has moved in PARENT after multiple pushes.
			self.link[KC_FRAME_ZERO].translation += poseChange_PARENT

			# accumulate pose change
			if KC_DEBUG:
				print "poseChange_ZERO", poseChange_ZERO
				exit()

			dpose = np.array([poseChange_ZERO[0], poseChange_ZERO[1], dtheta])
			self.accumPoseChange(dpose)

		# non-holonomic
		else:

			# special handling of NO_PARENT_TRANSLATION
			if push.flags & KC_PUSH_FLAG_NO_PARENT_TRANSLATION:

				"""
				Special treatment for NO_PARENT_TRANSLATION. We can't run the
				full algo because it's unstable unless we do the dr component
				that comes out of it.

				# push ZERO in PARENT - NO NO NO!!!
				(push.pos, push.vec) = self.link[KC_FRAME_ZERO].push(push.pos, push.vec)

				Instead, since we are only correcting gaze
				direction in azimuth in this case, we use a different tack.
				Having completed the push through the system, we transform
				the remaining pushvec back up into HEAD. There, we measure
				the azimuthal error remaining. We implement that in ZERO,
				which will approximately correct it in HEAD, also.

				NB: We could compute it exactly, it's not very onerous, but
				I think this computation is a pretty good approx providing
				the gaze range is large, and it's also I think stable, giving
				a slightly smaller dtheta than the correct computation.
				"""

				# transform pushpos and pushvec back up to HEAD
				push.pos = self.changeFrameAbs(KC_FRAME_ZERO, self.linkTip, push.pos);
				push.vec = self.changeFrameRel(KC_FRAME_ZERO, self.linkTip, push.vec);

				# compute pushed point P' = P + V
				pushed_pos = push.pos + push.vec

				# measure azimuth of P and P_prime
				a = np.arctan2(push.pos[1], push.pos[0])
				b = np.arctan2(pushed_pos[1], pushed_pos[0])

				# difference is change in azimuth to bring them in line
				dtheta = b - a
				self.link[KC_FRAME_ZERO].angle += dtheta

				# accumulate pose change
				self.accumPoseChange(np.array([0.0, dtheta]))

			else:

				"""
				Non-holonomic treatment; see Fig dev/kc/kc.png.

				To solve for a non-holonomic (diff. drive) platform, we can only
				move the platform exactly forwards or backwards, after making a
				rotation of our choice. Therefore, our goal is to rotate the
				platform so that the pushpoint P is directly "behind" (in the
				robot's eyes) the target point P' = P + V, with V the pushvec.
				Having done this, we can then push the platform forward by an
				amount "dx" until P arrives at P'. We label the image of P' after
				the rotation of the robot Q. We know that Q has the same
				y coordinate as P, because that is its definition (directly
				behind or in front of P means P.y == Q.y). We also know it has
				the same distance from the origin as P' ("r"), because it's a rotation
				of P' around the origin (the axis centre, for ZERO). From these
				two bits of information, we can determine its x coordinate; from
				that, we can determine its distance ahead of P, which will
				be our "dx" for ZERO. Finally, we can recover the rotation
				needed as equal to "c", with c = a - b (aka dtheta for ZERO).
				"""

				# P is the pushpos and V is the pushvec
				P = push.pos;
				V = push.vec;

				# compute pushed point P' = P + V
				P_prime = P + V

				# measure P' radius, denoted r in Fig NH
				r = np.sqrt(P_prime[0]*P_prime[0] + P_prime[1]*P_prime[1])

				# compute pushed point after rotation by the desired amount
				# which we have denoted Q
				Q_y = P[1]
				Q_x_sq = r*r - Q_y*Q_y

				# NB: There are pushes we will not be able to satisfy. See Fig
				# NH-FAIL, where r is smaller than y. In this case, x_sq will
				# come out as -ve. I intend to take measures in the MPG to make
				# sure this doesn't happen in practice; if it makes it this far,
				# rather than take the sqrt(-ve) and throw a NaN in the works,
				# we'll just abandon the computation and raise a warning.
				if Q_x_sq < 0:

					# warn and ignore
					miro.utils.warning("XSQ_NEGATIVE")

				else:

					# compute rotation required to achieve that
					Q_x = np.sqrt(Q_x_sq)
					b = np.arctan2(P_prime[1], P_prime[0])
					a = np.arctan2(Q_y, Q_x)
					c = b - a

					# derive pose change
					dr = Q_x - P[0]
					dtheta = c

					# apply
					self.link[KC_FRAME_ZERO].angle += dtheta
					theta = self.link[KC_FRAME_ZERO].angle
					self.link[KC_FRAME_ZERO].translation += dr * np.array([np.cos(theta), np.sin(theta), 0.0])

					# accumulate pose change
					self.accumPoseChange(np.array([dr, dtheta]))




