#!/bin/bash

# explicit list
echo -e "/model/cam0/compressed"
echo -e "/model/cam1/compressed"
echo -e "/overcam/image_mono/compressed"
echo -e "/whiskeye/head/bridge_u"
echo -e "/whiskeye/head/theta_cmd"
echo -e "/model/xy_q"
echo -e "/model/protracting"
echo -e "/body/pose"
echo -e "/whiskeye/body/odom"
exit 0




# initial list
cp /tmp/topics_all /tmp/topics
#rostopic list > /tmp/topics

# pare down
cat /tmp/topics \
	| grep -v "whiskeye/head/cam" \
	| grep -v "/overcam" \
	| grep -v "whiskeye/body/[^o][^d]" \
	| grep -v "/tf" \
	| grep -v "/rosout" \
	| grep -v "/rqt" \
	| grep -v "/distance" \
	| grep -v "/max_macon" \
	| grep -v "robotino" \
	| grep -v "head/log" \
	| grep -v "model/mapval" \
	| grep -v "model/sausage" \
	| grep -v "model/ior" \
	| grep -v "model/fin" \
	| grep -v "model/head_center" \
	| grep -v "model/theta" \
	| grep -v "model/tick" \
	| grep -v "model/visited" \
	| grep -v "model/contacted" \
	| grep -v "model/xy$" \
	> /tmp/topics_log

# add some back
echo -e "/whiskeye/body/cmd_vel" >> /tmp/topics_log
echo -e "/overcam/image_mono/compressed" >> /tmp/topics_log

# report
echo "\n----------------------------------------"
cat /tmp/topics_log
