#!/bin/bash

# function to kill
killrep()
{
	echo -e "--------------------------------"
	echo -e "waiting for kill \"$1\"..."
	while true
	do
		PID=`pgrep $1`
		[[ "$PID" == "" ]] && break
		sleep 0.1
	done
	echo -e "(done)\n"
}

# function to kill all
killthemall()
{
	killall model_main.py
	killall camera_filter.py
}

# if log was requested
LOG=
if [[ "$1" == "log" ]] ; then

	LOG=1
	echo using log...
	shift

	# kill existing
	killthemall
	
	# killrep all
	killrep camera_filter
	killrep model_main
	
	# ensure camera_filter
	echo -e "starting camera_filter..."
	./camera_filter.py &
	sleep 1

	# run model in bg (with any remaining arguments)
	./model_main.py $@ &
	
	# wait for start
	echo -en "waiting for start..."
	while true
	do
		LIST=`rostopic list | grep -v platform/cam`
		FIN=`echo $LIST | grep model/fin`
		[[ "$FIN" != "" ]] && break
		echo -en "."
		sleep 0.01
	done
	echo " "
	
	# get full list of topics to rosbag
	#LIST=`rostopic list | grep -E "(/model|/whiskeye|/body)" | grep -v "platform/cam"`
	#rostopic list | grep -v "whiskeye/head/cam" | grep -v "/overcam/" | grep -v "distance_sensors_clearing" | grep -v "mapval" | grep -v "/ior" | grep -v "sausage" > /tmp/topics
	LIST=`./log_topic_list.sh`
	echo "-----"
	echo $LIST
	echo "-----"

	# check for overhead camera topic
	TEST=`rostopic list | grep "^/body/pose"`
	[[ "$TEST" == "" ]] && { killthemall; sleep 1; echo -e "\n\n\noverhead camera not running"; exit 1; }
	
	# run rosbag
	rosbag record -O ~/yaca-log-`date +%y%m%d-%H%M%S` $LIST

	# killrep all
	sleep 1
	killrep camera_filter
	killrep model_main
	


# otherwise
else
	# run model (with any remaining arguments)
	./model_main.py $@

fi





